enum UserType{
  USER, EMERGENCY, ADMIN
}

class User {
  String id;
  String fullName;
  String phone;
  String password;
  String bloodType;
  String userType;

  User(
      {this.id,
        this.fullName,
        this.phone,
        this.password,
        this.bloodType,
        this.userType});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    phone = json['phone'];
    password = json['password'];
    bloodType = json['blood_type'];
    userType = json['user_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['blood_type'] = this.bloodType;
    data['user_type'] = this.userType;
    return data;
  }
}