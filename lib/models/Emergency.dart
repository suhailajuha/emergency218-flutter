enum Status{
  NEW, PROCESSING, DONE
}

class Emergency {
  String id;
  String description;
  String address;
  String emergencyType;
  String lat;
  String lng;
  String status;
  String userId;
  String userName;
  String userPhone;
  String userBloodType;
  String createdAt;
  String report;
  String reporterId;
  String reporterName;
  String reporterPhone;
  String reporterType;
  String reportAt;

  Emergency(
      {this.id,
        this.description,
        this.address,
        this.emergencyType,
        this.lat,
        this.lng,
        this.status,
        this.userId,
        this.userName,
        this.userPhone,
        this.userBloodType,
        this.createdAt,
        this.report,
        this.reporterId,
        this.reporterName,
        this.reporterPhone,
        this.reporterType,
        this.reportAt});

  Emergency.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    address = json['address'];
    emergencyType = json['emergency_type'];
    lat = json['lat'];
    lng = json['lng'];
    status = json['status'];
    userId = json['user_id'];
    userName = json['user_name'];
    userPhone = json['user_phone'];
    userBloodType = json['user_blood_type'];
    createdAt = json['created_at'];
    report = json['report'];
    reporterId = json['reporter_id'];
    reporterName = json['reporter_name'];
    reporterPhone = json['reporter_phone'];
    reporterType = json['reporter_type'];
    reportAt = json['report_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['description'] = this.description;
    data['address'] = this.address;
    data['emergency_type'] = this.emergencyType;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['status'] = this.status;
    data['user_id'] = this.userId;
    data['user_name'] = this.userName;
    data['user_phone'] = this.userPhone;
    data['user_blood_type'] = this.userBloodType;
    data['created_at'] = this.createdAt;
    data['report'] = this.report;
    data['reporter_id'] = this.reporterId;
    data['reporter_name'] = this.reporterName;
    data['reporter_phone'] = this.reporterPhone;
    data['reporter_type'] = this.reporterType;
    data['report_at'] = this.reportAt;
    return data;
  }
}

