import 'dart:math';

import 'package:emergency218/pages/emergency-list.dart';
import 'package:emergency218/pages/emergency-new.dart';
import 'package:emergency218/pages/landingEmergency.dart';
import 'package:emergency218/pages/landingUser.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:async';
import 'dart:convert';

import 'config.dart';
import 'firebase_notification_handler.dart';
import 'models/User.dart';
import 'pages/emergency-details.dart';
import 'pages/emergency-map.dart';
import 'pages/emergency-my-list.dart';
import 'pages/help.dart';
import 'pages/profile.dart';
import 'pages/signup.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Emergency 218',
      theme: ThemeData(
        fontFamily: 'Cairo-SemiBold',
        primarySwatch: Colors.red,
            ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("ar", ""), // OR Locale('ar', 'AE') OR Other RTL locales
      ],
      locale: Locale("ar", ""),
      home: MyHomePage(),
      routes: <String, WidgetBuilder>{
        '/signup': (BuildContext context) => new SignupPage(),
        '/landing-emergency': (BuildContext context) => new LandingEmergencyPage(),
        '/landing-user': (BuildContext context) => new LandingUserPage(),
        '/home': (BuildContext context) => new MyHomePage(),
        '/help': (BuildContext context) => new HelpPage(),
        '/emergency-my-list': (BuildContext context) => new EmergencyMyList(),
        '/emergency-list': (BuildContext context) => new EmergencyList(),
        '/emergency-details': (BuildContext context) => new EmergencyDetails(),
        '/emergency-map': (BuildContext context) => new EmergencyMap(),
        '/emergency-new': (BuildContext context) => new EmergencyNew(),
        '/profile': (BuildContext context) => new ProfilePage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController phone = new TextEditingController();

  TextEditingController password = new TextEditingController();

  @override
  void initState() {
    new FirebaseNotifications(context).setUpFirebase();

//    FirebaseNotifications(context).sendNotification('fff','fff');

    getPermissions();
    getPhone().then((result) {
      setState(() {
        phone.text = result;
      });
    });

    getPassword().then((result) {
      setState(() {
        password.text = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red.shade800,
        title: Text('طوارئ 218'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/emergency2.png',
              scale: 3,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Divider(),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: phone,
                decoration: new InputDecoration(
                  labelText: 'رقم الهاتف',
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: password,
                obscureText: true,
                decoration: new InputDecoration(
                  labelText: 'كلمة المرور',
                ),
              ),
            ),
            RaisedButton(
              onPressed: () => login(),
              child: Text("تسجيل الدخول"),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/signup');
              },
              child: Text(
                'ليس لديك حساب ؟ اضغط هنا ',
                style: TextStyle(
                  color: Colors.red.shade800,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  login() async {
    try {
      var response = await http.post(API_URL + "users/login.php",
          body: {"phone": phone.text, "password": password.text});

      var userJson = json.decode(response.body);
      User user = User.fromJson(userJson[0]);

      if (user == null) {
        Fluttertoast.showToast(msg: "رقم هاتف او كلمة مرور خاطئة");
      } else {
        print(user.toJson());

        if (user.userType == 'USER') {
          Navigator.pushReplacementNamed(context, '/landing-user');
        } else {
          Navigator.pushReplacementNamed(context, '/landing-emergency');
        }

        Fluttertoast.showToast(msg: "تم الدخول");

        saveToPref(user.id, user.userType, phone.text, password.text);
      }
    } catch (err) {
      Fluttertoast.showToast(msg: "حدث خطأ");
    }
  }

  getPermissions() async {
    await PermissionHandler().requestPermissions([
      PermissionGroup.camera,
      PermissionGroup.microphone,
      PermissionGroup.locationAlways
    ]);
  }

  saveToPref(userId, userType, phone, password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phone);
    prefs.setString('password', password);
    prefs.setString('userId', userId);
    prefs.setString('userType', userType);
  }

  getPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('phone');
  }

  getPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('password');
  }
}
