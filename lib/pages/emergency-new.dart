import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import '../firebase_notification_handler.dart';

class EmergencyNew extends StatefulWidget {
  @override
  _EmergencyNewState createState() => _EmergencyNewState();
}

class _EmergencyNewState extends State<EmergencyNew> {
  TextEditingController description = new TextEditingController();
  TextEditingController address = new TextEditingController();
  String emergencyType = 'FIRE';
  String lat;
  String lng;

  save() async {
    print(lat);
    print(await getUser());

    try {
      var resp = await http.post(API_URL + "emergency/new.php", body: {
        "description": description.text,
        "address": address.text,
        "emergency_type": emergencyType,
        "lat": lat,
        "lng": lng,
        "created_by": await getUser(),
      });

      print(resp.body);

      if (resp.body == 'ok') {
        Fluttertoast.showToast(msg: "تم اضافة بلاغ");
        new FirebaseNotifications(context).setUpFirebase();
        FirebaseNotifications(context)
            .sendNotification(emergencyType, description.text);
      } else {
        Fluttertoast.showToast(msg: "فشل اضافة البلاغ");
      }
    } catch (err) {
      Fluttertoast.showToast(msg: "حدث خطأ");
      print(err);
    }
  }

  getLocation() async {
    var location = new Location();

    location.onLocationChanged().listen((LocationData current) {
      lat = current.latitude.toString();
      lng = current.longitude.toString();
      print(current.latitude);
      print(current.longitude);
    });
  }

  @override
  void initState() {
    super.initState();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('بلاغ جديد'),
        backgroundColor: Colors.red.shade800,
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: description,
                decoration: new InputDecoration(
                  labelText: 'التفاصيل',
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: address,
                decoration: new InputDecoration(
                  labelText: 'العنوان',
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: DropdownButton<String>(
                items: <String>['FIRE', 'ACCEDENT', 'CRIME']
                    .map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    emergencyType = value;
                  });
                },
                value: emergencyType,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30, right: 30, top: 8, bottom: 16),
              child: RaisedButton(
                onPressed: () => save(),
                child: Text("حفظ"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('userId');
  }
}
