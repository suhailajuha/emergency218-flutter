import 'dart:async';
import 'dart:convert';

import 'package:emergency218/models/Emergency.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../config.dart';
import 'package:http/http.dart' as http;

class EmergencyMap extends StatefulWidget {
  @override
  _EmergencyMapState createState() => _EmergencyMapState();
}

class _EmergencyMapState extends State<EmergencyMap> {
  Completer _controller = Completer();

  Set<Marker> markers;

  Future<List<Emergency>> getData() async {
    final response = await http.get(API_URL + "emergency/getAll.php");
    Iterable l = json.decode(response.body);
    List<Emergency> emergencyList =
    l.map((model) => Emergency.fromJson(model)).toList();

    setState(() {
      markers = emergencyList
          .map(
            (emergency) => new Marker(
          markerId: MarkerId(emergency.id),
          position: LatLng(double.parse(emergency.lat), double.parse(emergency.lng)),
          infoWindow: InfoWindow(
              title: emergency.emergencyType, snippet: emergency.status),
          onTap: () {},
        ),
      ).toSet();
    });

    print(markers);

    return emergencyList;
  }
  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text('خريطة البلاغات'),
        backgroundColor: Colors.red.shade800,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              height: 500.0,
              child: GoogleMap(
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: LatLng(32.888472, 13.187011),
                  zoom: 14,
                ),
                markers: markers,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
