import 'dart:async';
import 'dart:convert';

import 'package:emergency218/models/Emergency.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';

class EmergencyDetails extends StatefulWidget {
  @override
  _EmergencyDetailsState createState() => _EmergencyDetailsState();
}

class _EmergencyDetailsState extends State<EmergencyDetails> {

  Set<Marker> markers;
  Completer _controller = Completer();
  String type;

  getUserType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      type = prefs.getString('userType');

    });
  }

  @override
  void initState() {
    super.initState();
    getUserType();
  }

  @override
  Widget build(BuildContext context) {
    final Emergency emergency = ModalRoute.of(context).settings.arguments;


    return Scaffold(
      appBar: AppBar(
        title: Text('التفاصيل'),
        backgroundColor: Colors.red.shade800,
      ),
      body: ListView(
        children: <Widget>[
          Text('رقم الحالة'),
          Text(emergency.id),
          Text('التفاصيل'),
          Text(emergency.description),
          Text('العنوان'),
          Text(emergency.address),
          Text('نوع البلاغ'),
          Text(emergency.emergencyType),
          Text('الحالة'),
          Text(emergency.status),
          Text('انشأت في'),
          Text(emergency.createdAt),
          Text('انشأت من قبل'),
          Text(emergency.userName),
          Text('رقم هاتف المبلغ'),
          Text(emergency.userPhone),
          Text('فصيلة دم المبلغ'),
          Text(emergency.userBloodType),
          Text('المكان على الخريطة'),
          isNew(emergency),
          Container(
            height: 500.0,
            child: GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                target: LatLng(double.parse(emergency.lat), double.parse(emergency.lng)),
                zoom: 16,
              ),
              markers: Set<Marker>.of(
                <Marker>[
                  Marker(
                    draggable: true,
                    markerId: MarkerId("1"),
                    position: LatLng(double.parse(emergency.lat), double.parse(emergency.lng)),
                    icon: BitmapDescriptor.defaultMarker,
                    infoWindow: const InfoWindow(),
                  )
                ],
              ),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget isNew(Emergency emergency){
    if(type != 'USER') {
      if (emergency.status == 'NEW') {
        return RaisedButton(child: Text('استقبال الحالة'), onPressed: () {},);
      } else if (emergency.status == 'PROCESSING') {
        return RaisedButton(
          child: Text('انهاء وكتابة تقرير'), onPressed: _showMaterialDialog,);
      } else {
        return Text('');
      }
    }else{
      return Text('');
    }
  }

  void _showMaterialDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('تقرير الحالة'),
            content: TextFormField(),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                  },
                  child: Text('الغاء')),
              FlatButton(
                onPressed: () {
                },
                child: Text('تأكيد'),
              )
            ],
          );
        });
  }
}
