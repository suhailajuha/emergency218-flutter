import 'dart:convert';

import 'package:emergency218/models/Emergency.dart';
import 'package:emergency218/models/args/ListArgs.dart';
import 'package:emergency218/utils/list_builder.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import 'package:http/http.dart' as http;

class EmergencyList extends StatefulWidget {
  @override
  _EmergencyListState createState() => _EmergencyListState();
}

class _EmergencyListState extends State<EmergencyList> {

  Future<List<Emergency>> getData(status) async {
    final response = await http.get(API_URL + "emergency/getAll.php?status=" + status + "&emergency_type=" + await getUserType());
    Iterable l = json.decode(response.body);
    List<Emergency> emergencyList =
        l.map((model) => Emergency.fromJson(model)).toList();
    return emergencyList;
  }

  @override
  Widget build(BuildContext context) {

    final String status = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('بلاغات'),
        backgroundColor: Colors.red.shade800,
      ),
      body: FutureBuilder<List<Emergency>>(
        future: getData(status),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ItemList(
                  emergencyList: snapshot.data,
                )
              : Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }
  getUserType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('userType');
  }
}

class ItemList extends StatelessWidget {
  final List<Emergency> emergencyList;
  ItemList({this.emergencyList});

  @override
  Widget build(BuildContext context) {
    return ListBuilder<Emergency>(
      items: emergencyList,
      builder: (emergency) {
        return Container(
          padding: const EdgeInsets.all(10.0),
          child: InkWell(
            onTap: () {

              Navigator.pushNamed(context, '/emergency-details', arguments: emergency);

            },
            child: Card(
              child: ListTile(
                title: Text(" نوع الحالة:  ${emergency.emergencyType}"),
                leading: Text(emergency.id + '#'),
                subtitle: Text(" انشأت في:  ${emergency.createdAt}"),
              ),
            ),
          ),
        );
      },
    );
  }
}
