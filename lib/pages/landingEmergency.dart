import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LandingEmergencyPage extends StatefulWidget {

  @override
  _LandingEmergencyPageState createState() => _LandingEmergencyPageState();
}

class _LandingEmergencyPageState extends State<LandingEmergencyPage> {
  String type = '';

  getUserType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      type = prefs.getString('userType');
    });
  }

  @override
  void initState() {
    super.initState();
    getUserType();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(type + ' '),
        backgroundColor: Colors.red.shade800,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.signOutAlt,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/home');
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-list', arguments: 'NEW');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.listAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('بلاغات جديدة')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-list', arguments: 'PROCESSING');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.listAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('بلاغات قيد التنفيذ')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-map');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.mapMarkedAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('خريطة البلاغات')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-list', arguments: 'DONE');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.listAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('بلاغات منتهية')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/profile');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.userCircle,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('حسابي')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/help');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.questionCircle,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('المساعدة'),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
