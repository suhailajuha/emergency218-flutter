import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'call.dart';

class LandingUserPage extends StatelessWidget {

  String userId;

  @override
  Widget build(BuildContext context) {
    userId = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('طوارئ 218'),
        backgroundColor: Colors.red.shade800,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.signOutAlt,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/home');
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-new');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.solidEdit,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('بلاغ جديد')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new CallPage(channelName: 'emergency218',)));
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.phoneAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('اتصال بمركز الطوارئ')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-map');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.mapMarkedAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('خريطة البلاغات')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/emergency-my-list');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.listAlt,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('بلاغاتي')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/profile');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.userCircle,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('حسابي')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/help');
                    },
                    child: Container(
                      height: 150,
                      child: Card(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 24.0, bottom: 8.0),
                                child: Icon(
                                  FontAwesomeIcons.questionCircle,
                                  color: Colors.red.shade800,
                                  size: 50.0,
                                ),
                              ),
                              Text('المساعدة'),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

}
