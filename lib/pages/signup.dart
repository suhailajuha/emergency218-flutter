import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import '../config.dart';


class SignupPage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignupPage> {


  TextEditingController phone = new TextEditingController();
  TextEditingController name = new TextEditingController();
  TextEditingController password = new TextEditingController();
  String blood = 'A+';

  signup() async {
    try {
      var resp = await http.post(API_URL + "users/signup.php", body: {
        "phone": phone.text,
        "name": name.text,
        "blood": blood,
        "password": password.text,
      });

      if (resp.body == 'ok') {
        Fluttertoast.showToast(msg: "تم التسجيل");
      } else {
        Fluttertoast.showToast(msg: "فشل التسجيل");
      }
    }catch (err) {
      Fluttertoast.showToast(msg: "حدث خطأ");
      print(err);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red.shade800,
        title: Text('طوارئ 218'),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: name,
                decoration: new InputDecoration(
                  labelText: 'الاسم التلاتي',
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: phone,
                decoration: new InputDecoration(
                  labelText: 'رقم الهاتف',
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: DropdownButton<String>(
                items: <String>['A+', 'A-', 'B+', 'B-','AB+','AB-','O+','O-'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    blood = value;
                  });
                },
                value: blood,
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: TextFormField(
                controller: password,
                obscureText: true,
                decoration: new InputDecoration(
                  labelText: 'كلمة المرور',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 8, bottom: 16),
              child: RaisedButton(
                onPressed: () => signup(),
                child: Text("تسجيل"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}