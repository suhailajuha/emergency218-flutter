import 'dart:convert';

import 'package:emergency218/models/Emergency.dart';
import 'package:emergency218/models/args/ListArgs.dart';
import 'package:emergency218/utils/list_builder.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import 'package:http/http.dart' as http;

class EmergencyMyList extends StatefulWidget {
  @override
  _EmergencyListState createState() => _EmergencyListState();
}

class _EmergencyListState extends State<EmergencyMyList> {

  Future<List<Emergency>> getData() async {
    final response = await http.get(API_URL + "emergency/getByUser.php?userId=" + await getUser() );
    Iterable l = json.decode(response.body);
    List<Emergency> emergencyList =
        l.map((model) => Emergency.fromJson(model)).toList();
    return emergencyList;
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text('بلاغات'),
        backgroundColor: Colors.red.shade800,
      ),
      body: FutureBuilder<List<Emergency>>(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ItemList(
                  emergencyList: snapshot.data,
                )
              : Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }

  getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('userId');
  }
}

class ItemList extends StatelessWidget {
  final List<Emergency> emergencyList;
  ItemList({this.emergencyList});

  @override
  Widget build(BuildContext context) {
    return ListBuilder<Emergency>(
      items: emergencyList,
      builder: (emergency) {
        return Container(
          padding: const EdgeInsets.all(10.0),
          child: InkWell(
            onTap: () {

              Navigator.pushNamed(context, '/emergency-details', arguments: emergency);
            },
            child: Card(
              child: ListTile(
                title: Text(" نوع الحالة:  ${emergency.emergencyType}"),
                leading: Text(emergency.id + '#'),
                subtitle: Text(" انشأت في:  ${emergency.createdAt}"),
              ),
            ),
          ),
        );
      },
    );
  }
}
