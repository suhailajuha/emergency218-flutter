import 'package:flutter/material.dart';

class PlaceHolderWidget extends StatelessWidget {
  final String message;

  const PlaceHolderWidget({Key key, this.message = "Nothing to show"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.list, size: 48),
          Text(message),
        ],
      ),
    );
  }
}
