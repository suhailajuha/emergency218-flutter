import 'package:flutter/cupertino.dart';

import 'place_holder_widget.dart';

class ListBuilder<T> extends StatelessWidget {
  final List<T> items;
  final Widget Function(T item) builder;
  final Widget placeHolder;
  final Axis direction;

  const ListBuilder({
    Key key,
    this.items,
    this.builder,
    this.placeHolder = const PlaceHolderWidget(),
    this.direction = Axis.vertical,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return items.isEmpty
        ? placeHolder
        : ListView.builder(
            scrollDirection: direction,
            itemCount: items.length,
            itemBuilder: (_, index) => builder(items[index]),
          );
  }
}
